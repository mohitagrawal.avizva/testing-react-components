import React from 'react';
import { forEach, isEmpty, get } from 'lodash';
import { runFieldValidations } from 'utils';
import { flatFormView as FlatFormView } from 'views/flatForm.view.jsx';
import moment from 'moment';

export default class FlatForm extends React.Component {

    constructor( props ) {

        super( props );

        this._bind();

        this.state = {
            fieldsData: this.composeStateFromProps( props ), //Complete information corresponding to all fields in the form.
            showErrors: false, //When true, all fields can show their respective error messages.
            isValid: false //Whether the form is valid as a whole.
        };
    }

    /**
     * @desc Takes the fieldsMap in the props and composes the initial state of the form according
     * to it and the selectedValues.
     * @param {*} payload
     */
    composeStateFromProps( payload ) {

        const { fieldsMap, selectedValues } = payload;
        const _state = {};

        forEach( fieldsMap, ( field, fieldId ) => {


			let _value;
			if( 'date' === field.type && isEmpty( selectedValues[ field.name ] ) ) {
				_value = moment().format( 'MM-DD-YYYY' );
			} else {
				_value = selectedValues[ field.name ];
			}

			const { isValid, errorMessage, criterionStatus } = this.validateField( field, _value );

            _state[ fieldId ] = {
                ...field,
                value: _value, // Selected value.
                isValid, // Whether the field is valid.
                errorMessage, // Current error message.
                displayValue: this.getFieldDisplayValue( _value, field.formatter ), // Formatted value visible to the user.
                fieldCriterionStatus: criterionStatus // Boolen flags corresponding to different criterions.
            };
        } );

        return _state;
    }

    /**
     * @desc Returns the display value of the a particular field.
     * @param {*} value
     * @param {*} formatter
     */
    getFieldDisplayValue( value, formatter ) {

        if ( isEmpty( formatter ) ) {

            // If value is undefined or null, then blank string needs to be set to avoid react errors.
            return value || '';
        } else {

            //@todo: Write formatting logic here.
            return value;
        }
    }

    /**
     * @desc Manages communication with the field validation utility.
     * @param {*} field
     * @param {*} value
     */
    validateField( field, value ) {

        const { validations, required, requiredMessage, criterion, config } = field;

        return runFieldValidations( {
            value: value, // Current value of the field.
            validations, // List of validations corresponding to the field.
            required, // If the field is mandatory.
            requiredMessage, // Mandatory error message.
            criterion, // Criterion name that the field needs to obey.
            config, // Additonal config corresponding to the field.

            // In case the validation utility needs to check another field's value for this field.
            getReferenceValue: ( fieldId ) => {
                return this.getFieldProperty( fieldId, 'value' );
            }
        } );
    }

    /**
     * @desc Based on the field id provided, returns the requested property's value.
     * @param {*} fieldId
     * @param {*} propertyName
     */
    getFieldProperty( fieldId, propertyName ) {

        let _field;

		if( isEmpty( this.state ) ) {
			_field = this.props.fieldsMap[ fieldId ];
		} else {
			_field = this.state.fieldsData[ fieldId ];
        }

        // in case field property is not found, try inside selectedValues or `null` value is returned.
        return get( _field, propertyName, get( this.props.selectedValues, fieldId, null ) );
    }

    /**
     * @desc Returns the entire object corresponding to the state.
     * Used by the fields' view to retrieve its props.
     * @param {*} fieldId
     */
    getField( fieldId ) {

        return get( this.state.fieldsData, fieldId, null ); //In case field is not found, null is returned.
    }

    /**
     * @desc Called when a user input is captured by the field.
     * @param {*} param0
     */
    handleFieldChange( { fieldId, fieldValue, validated, ...remainingData } ) {

        const { fieldsMap } = this.props;

        const _field = fieldsMap[ fieldId ];

        let _updateObject = {};


        if ( ! validated ) { //If the field is not already validated, carries out validation process for it.

            const _validationObj = this.validateField( _field, fieldValue ); //Validation response.

            _updateObject = {
                ...this.state.fieldsData[ fieldId ],
                value: fieldValue, //new value.
                isValid: _validationObj.isValid, //updated validation status.
                errorMessage: _validationObj.errorMessage, //updated error message.
                displayValue: this.getFieldDisplayValue( fieldValue, _field.formatter ), //updated display value.
                fieldCriterionStatus: _validationObj.criterionStatus //criterion status.
            };

        } else { //If the field is already validated, simply updates the attributes.

            const { isValid, errorMessage, criterionStatus } = remainingData;

            _updateObject = {
                ...this.state.fieldsData[ fieldId ],
                value: fieldValue, //new value.
                isValid, //updated validation status.
                errorMessage, //updated error message.
                displayValue: this.getFieldDisplayValue( fieldValue, _field.formatter ), //updated display value.
                fieldCriterionStatus: criterionStatus //criterion status.
            };

        }

        //Finally, update the state.
        this.setState( {
            fieldsData: {
                ...this.state.fieldsData,
                [ fieldId ]: {
                    ..._updateObject
                }
            }
        }, () => {
			this.checkForAdditionalCallbacks( fieldId );
			this.alsoValidateOnChange( fieldId );
		} );
	}

	//live validation on field change
	alsoValidateOnChange( fieldId ) {

		const _fieldsData = this.state.fieldsData;

		const field = _fieldsData[ fieldId ];

		const config = get( field, 'config', {} );

		//check if config and alsoValidateOnChange is not empty, else do nothing.
		if( !isEmpty( config ) && !isEmpty( config.alsoValidateOnChange ) ) {

			//iterate over every element in alsovalidateOnChange and call handleFieldChange for those elements
			return config.alsoValidateOnChange.map( item => {
				const _referedField = _fieldsData[ item ];

					//check if _referedField exists. i.e. it may not be the part of current form.
					if( !isEmpty( _referedField ) ) {
						this.handleFieldChange( {
							..._referedField,
							fieldId: _referedField.id,
							fieldValue: _referedField.value
						} );
					}
			} );
		}

	}

	/**
	 * @description in case there are any additional callbacks for the field then return the callback with fieldsData of the field
	 * @param {*} fieldId
	 */
	checkForAdditionalCallbacks( fieldId ) {
		if ( this.props.additionalCallbacksMap && this.props.additionalCallbacksMap.hasOwnProperty( fieldId ) ) {
			if ( 'function' === typeof this.props.additionalCallbacksMap[ fieldId ] ) {
				this.props.additionalCallbacksMap[ fieldId ]( this.state.fieldsData[ fieldId ] );
			} else {
				console.warn( `No additional callback found for the field id ${fieldId}` );
			}
		}
	}

    /**
     * @desc Called when the user attempts to submit the form.
     * @param {*} event
     */
    handleSubmit( event ) {

        if( event ) {
            event.preventDefault(); // prevent the default submit behavior.
        }

        let _key;

        const { fieldsData } = this.state;

        let _isValid = true;

        for ( _key in fieldsData ) { //Iterate over all the fields in state.
            if ( fieldsData.hasOwnProperty( _key ) ) {
                if ( ! fieldsData[ _key ].isValid ) { //If any field is found invalid,
                    _isValid = false; //Mark the form as invalid and break the loop.
                    break;
                }
            }
        }

        //Update the status accordingly.
        this.setState( {
            isValid: _isValid,
            showErrors: true
        }, this.executeCallback );
    }

    /**
     * @desc Executes the callback provided to it by the parent container.
     */
    executeCallback() {

        // If the form is valid and the callback is a function, then pass the control back to the parent container.
        if ( ( this.props.submitIfInvalid || this.state.isValid ) && 'function' === typeof this.props.callback ) {
            this.props.callback(
                this.normalizeFormData()
            );
        }
    }

    /**
     * @desc Iterates over the fieldsData object in the state,
     * creates a new normalized object that contains the field's value against its name.
     */
    normalizeFormData() {

        const { fieldsData } = this.state;

        const _normalizedData = {};

        forEach( fieldsData, ( field ) => {
            _normalizedData[ field.name ] = field.value;
        } );

        return _normalizedData;
    }

    /**
     * @desc Renders the form module.
     */
    render() {

        const _contextValue = {
            fieldsData: this.state.fieldsData, //Data of the fields.
            onChange: this.handleFieldChange, //On change handler.
            validateField: this.validateField, //Field validation method.
            showErrors: this.state.showErrors, //If the fields are allowed to show error messages.
            getField: this.getField, //For field to fetch its model.
            getFieldProperty: this.getFieldProperty //To get the property value of a particular field.
        };

        const { action, method, name, encType, className, id, field } = this.props;

        const formAttributes = {
            action,
            method,
            name,
            encType,
            className,
            id,
            field,
            onSubmit: this.handleSubmit
        };

        /**
         * Wrap the entire form in the provider provided by the parent container.
         * The form becomes the consumer.
         * @todo: In future, there can be a possibility that the parent container sets some initial
         * properties in the context and then the form adds its own information to the same context.
         */
        return (
            <this.props.provider value = { _contextValue }>
                <this.props.consumer>
                    {
                        ( props ) => {
                            return (
                                <FlatFormView
                                    consumer = { this.props.consumer }
                                    { ...formAttributes }
                                    fieldsOrder = { this.props.fieldsOrder }
                                    fieldsMap = { this.props.fieldsMap }
									{ ...props }
                                />
                            );
                        }
                    }
                </this.props.consumer>
            </this.props.provider>
        );
    }

    /**
     * @desc Bind events and methods to instance
     */
    _bind() {

        this.composeStateFromProps = this.composeStateFromProps.bind( this );
        this.validateField = this.validateField.bind( this );
        this.handleFieldChange = this.handleFieldChange.bind( this );
        this.handleSubmit = this.handleSubmit.bind( this );
        this.executeCallback = this.executeCallback.bind( this );
        this.getFieldProperty = this.getFieldProperty.bind( this );
		this.getField = this.getField.bind( this );
        this.alsoValidateOnChange = this.alsoValidateOnChange.bind( this );
    }

    /**
     * @desc clear events and bindings
     */
    componentWillUnmount() {
    }
}

FlatForm.displayName = 'FlatForm';
