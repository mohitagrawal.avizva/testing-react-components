export const fieldValidationNames = {
    REQUIRED: 'required',
    ONE_UPPER_CASE: 'oneUppercase',
    ONE_NUMERIC: 'oneNumeric',
	MINLENGTH: 'minlength',
	MAXLENGTH: 'maxlength',
    NOT_EQUAL_TO_FIELD: 'notEqualToField',
    ZIPCODE: 'zipCode',
    PHONE: 'phone',
    EMAIL: 'email',
    LAST_4_SSN: 'last4Ssn',
	EQUAL_TO_FIELD: 'equalToField',
	EQUAL_TO_CASE_IN_SENSITIVE: 'equalToCaseInsensitive'
};
