export const VALUE = 'value';

export const DISPLAY_VALUE = 'displayValue';

export const ERROR_MESSAGE = 'errorMessage';

export const SUBMIT = 'submit';

export const FIELD_TYPE = {
	DATE: 'date',
	TEXT: 'text',
	PASSWORD: 'password',
	NUMBER: 'number',
	TOKEN: 'token',
	TOKEN_ALTERNATE: 'tokenAlternate',
	RADIO: 'radio'

};

export const RADIO_TYPE = {
	RADIO_BUTTON: 'radioButton',
	RADIO_BUTTON_WITH_ICON: 'radioButtonWithIcon'
};
