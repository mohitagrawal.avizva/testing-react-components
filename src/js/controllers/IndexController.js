import React from 'react';
import ReactDOM from 'react-dom';
import { fieldsMap, fieldsOrder } from 'models';
import Flatform from 'components/Flatform.jsx';

export class IndexController {

  constructor( element, instanceId ) {
    this.el = element; //Cache the DOM node.

    this.instanceId = instanceId; //ID of the instance.
    // component data
    this.context = React.createContext( {} );

    this.data = { };

    this.bind();

    this.getFormData();

    this.render(); //Render into the DOM.
  }

  getFormData() {
    this.data = {
      method: 'POST',
      fieldsMap: fieldsMap,
      fieldsOrder: fieldsOrder,
      selectedValues: {
        username: '',
        confirmUsername: '',
        password: '',
        confirmPassword: '',
        zipCode: ''
      },
      callback: this.onCompleteCallback,
      key: 'form',
      id: 'form',
      provider: this.context.Provider,
      consumer: this.context.Consumer
    };
  }

  render() {
    ReactDOM.render( <Flatform { ...this.data }/>, this.el );
  }

  onCompleteCallback( paylaod ) {
    console.log( 'Form Submitted SuccessFully', paylaod );
  }

  bind() { 
    this.onCompleteCallback = this.onCompleteCallback.bind( this );
  }
}

IndexController.instanceCount = 0;
