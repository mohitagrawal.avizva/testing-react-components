import { controllersMap } from './controllersMap';

const onLoadHandler = () => {
  const instanceMap = {}; //Map to store all instances.

  const nameSpace = 'data-controller'; //Namespace to search for.

  const domNodes = document.querySelectorAll( `[${nameSpace}]` ); //All DOM nodes under the namespace.

  let counter = 0; //Initialization of the counter.

  const totalNodes = domNodes.length; //Total number of the controller nodes.

  for ( counter ; counter < totalNodes ; counter = counter + 1 ) { //Iterate over the nodes.

    const domNode = domNodes[ counter ]; //Each node.

    const controllerName = domNode.getAttribute( `${nameSpace}` ); //Get the controller class name.

    const ClassRefrence = controllersMap[ controllerName ]; //Reference of the class.

    if ( undefined === instanceMap[ controllerName ] ) {  //If no namespace in the instance map, add one.

      instanceMap[ controllerName ] = {};
    }

    const currentInstanceId = ClassRefrence.instanceCount; //ID of the instance.

    const controllerInstance = new ClassRefrence( domNode, currentInstanceId ); //Initialization of the instance.

    instanceMap[ controllerName ][ currentInstanceId ] = controllerInstance; //Map of each class's instances.

    ClassRefrence.instanceCount = ClassRefrence.instanceCount + 1; //Update instance count for next time.
  }
};

document.addEventListener( 'DOMContentLoaded', onLoadHandler, false ); //Equivalent of $( document).on( 'ready', function() {}, false )
