import React from 'react';
import classNames from 'classnames';
import { isEmpty } from 'lodash';
import { textFieldView as TextFieldView } from 'views/textField.view.jsx';
import { titleView as TitleView } from 'views/title.view.jsx';

/**
 * @type {object}
 * @desc A React presentation/dumb view component.
 * @example
 * import { inputFieldView } from 'views/shared/formElements/inputField.view';
 *
 * // render
 * <InputFieldView propName='propValue'/>
 */
export const inputFieldView = ( props ) => {

    /**
     * @desc concats additional class from props if any.
     */
    const _className = classNames( 'input__field__wrapper', {
        ...props.className
    } );

    /**
     * @desc concats modifiers from props if any.
     */
    const _dynamicClass = classNames( 'input__field', {
        'input__field--flat': props.flatView,
        'input__field--disabled': props.disabled,
        'input__field--readonly': props.readOnly,
        'input__field--error': props.showErrors && ! isEmpty( props.errorMessage )
    } );
    
    const labelClass = classNames( 'input__field__fieldset__label', {
        'input__field__fieldset__label--flat': props.flatView
    } );

    return (
        <div key = { props.key } className = { _className }>
            <fieldset className = 'input__field__fieldset'>
                <TitleView
                    tagName = 'label'
                    level = { 3 }
                    value = { props.displayLabel }
                    className={ labelClass }
                />
                <TextFieldView
                    className = { _dynamicClass }
                    value = { props.displayValue }
                    { ...props }
                />
            </fieldset>

            {
                props.showErrors && ! isEmpty( props.errorMessage )  ? (
                    <p className='input__field--error-message'> { props.errorMessage } </p>
                ) : null
            }
        </div>
    );
};

// default props
inputFieldView.defaultProps = {
    type: 'text',
    displayLabel: '',
    required: false,
    readOnly: false,
    placeholder: '',
    showErrors: false,
    autocomplete: 'off'
};
