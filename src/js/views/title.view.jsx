import React from 'react';
import classNames from 'classnames';

// get tag name
const getTagName = ( level ) => {
    if( 1 === level ) {
        return 'h1';
    } else if( 2 === level ) {
        return 'h2';
    } else if( 3 === level ) {
        return 'h3';
    } else if( 4 === level ) {
        return 'h4';
    } else if( 5 === level ) {
        return 'h5';
    } else if( 6 === level ) {
        return 'h6';
    }
};

/**
 * @type {object}
 * @desc A React presentation/dumb view component.
 * @example
 * import { titleView } from 'views/title.view';
 *
 * // render
 * <TitleView propName='propValue'/>
 */
export const titleView = ( props ) => {

    /**
     * @description adds classes based on different levels and additional classNames if any.
     */
    const _className = classNames( `heading-level--${ props.level }`, props.className );
    const TagName = props.tagName ? props.tagName : getTagName( parseInt( props.level ) );
    
    return (
        <TagName className={ _className } { ...props.additionalAttributes }>
            { props.value }
        </TagName>
    );
};
titleView.defaultProps = {
    level: 1,
    additionalAttributes: {}
};
