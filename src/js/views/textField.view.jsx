import React from 'react';
import { isFunction } from 'lodash';

/**
 * @type {object}
 * @desc A React presentation/dumb view component.
 * @example
 * import { textFieldView } from 'views/shared/formElements/textField.view';
 *
 * // render
 * <TextFieldView propName='propValue'/>
 */
export const textFieldView = ( props ) => {
    return (
        <input
            className = { props.className }
            type = { props.type }
            placeholder = { props.placeholder }
            onChange = { ( event ) => {
                if( event.target.value && props.maxlength && ( event.target.value.length > props.maxlength ) ) {
                    event.preventDefault();
                    return false;
                } else {
                    props.onChange( {
                        fieldId: props.id,
                        fieldValue: event.target.value,
                        ...props
                    } );
                }
            } }
            required = { props.isRequired }
            readOnly = { props.readOnly }
            onBlur = { ( event ) => {
                if( isFunction( props.onBlur ) ) {
                    props.onBlur( event );
                }
            } }
            id = { props.id }
            name = { props.name }
			value = { props.value || '' }
			maxLength = { props.maxlength }
            autoComplete = { props.autocomplete }
            { ...props.additionalAttributes }
        />
    );
};
