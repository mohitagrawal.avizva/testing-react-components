import React from 'react';
import { COMMON, FIELD_PROPERTY_NAMES } from 'constants';
import { passwordFieldView as PasswordFieldView } from './passwordField.view.jsx';
import { inputFieldView as InputFieldView } from './inputField.view.jsx';

const renderFields = ( fieldsOrder, Consumer, getField ) => {

    return fieldsOrder.map( ( fieldId ) => {
		const _field = getField( fieldId );
		const _fieldsToRender =[];

        switch( _field.type ) {
            case FIELD_PROPERTY_NAMES.FIELD_TYPE.TEXT:
            case FIELD_PROPERTY_NAMES.FIELD_TYPE.NUMBER:
                _fieldsToRender.push(
                    <Consumer key = { `consumer-${ fieldId }` }>
                        {
                            ( props ) => {
                                return (
                                    <InputFieldView
                                        onChange = { props.onChange }
                                        showErrors = { props.showErrors }
                                        { ..._field }
                                        getFieldProperty = { props.getFieldProperty }
                                    />
                                );
                            }
                        }
                    </Consumer>
                );

                break;
			case FIELD_PROPERTY_NAMES.FIELD_TYPE.PASSWORD: {
				_fieldsToRender.push(
                    <Consumer key = { `consumer-${ fieldId }` }>
                        {
                            ( props ) => {
                                return (
                                    <PasswordFieldView
                                        { ..._field }
                                        onChange = { props.onChange }
                                        showErrors = { props.showErrors }
                                        getFieldProperty = { props.getFieldProperty }
                                    />
                                );
                            }
                        }
                    </Consumer>
                );
				break;
            }
            default: {
                _fieldsToRender.push();
                break;
            }
        }

        return _fieldsToRender;
    } );
};

/**
 * @type {object}
 * @desc A React presentation/dumb view component.
 * @example
 * import { flatFormView } from 'views/shared/formElements/flatForm.view';
 *
 * // render
 * <FlatFormView propName='propValue'/>
 */
export const flatFormView = React.forwardRef( ( props, forwardedRef ) => {
    const {
        className, action, method, name, id,
        field, encType, onSubmit, fieldsOrder,
		consumer, getField
    } = props;

    return (
        <form
            className = { className }
            action = { action }
            method = { method }
            name = { name }
            id = { id }
            field = { field }
            encType = { encType }
			onSubmit = { onSubmit }
			ref = { forwardedRef }
        >
            {
                renderFields(
                    fieldsOrder,
                    consumer,
                    getField
                )
            }
            <button type='submit' className = 'form-button' >
                Submit
            </button>
        </form>
    );
} );

// default props
flatFormView.defaultProps = {
    className: 'action-info__content',
    method: COMMON.REQUEST_POST,
    encType: 'application/x-www-form-urlencoded'
};

// export view with default animations
flatFormView.displayName = 'flatFormView';
