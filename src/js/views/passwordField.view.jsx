import React from 'react';
import classNames from 'classnames';
import { textFieldView as TextFieldView } from 'views/textField.view.jsx';
import { titleView as TitleView } from 'views/title.view.jsx';
import { getCriterionModel } from 'utils/getCriterionModel';
import { isEmpty } from 'lodash';

/**
 * @type {object}
 * @desc A React presentation/dumb view component.
 * @example
 * import { passwordFieldView } from 'views/shared/formElements/passwordField.view';
 *
 * // render
 * <PasswordFieldView propName='propValue'/>
 */
export const passwordFieldView = ( props ) => {

     /**
     * @description concats additional class from props if any.
     */
    const _className = classNames( 'password__field__wrapper', {
        ...props.className
    } );

    /**
     * @description concats modifiers from props if any.
     */
    const _dynamicClass = classNames( 'password__field', {
        'password__field--flat': props.flatView,
        'password__field--disabled': props.disabled,
        'password__field--readonly': props.readOnly,
        'password__field--error': props.showErrors && ! props.isValid
    } );


    const labelClass = classNames( 'password__field__fieldset__label', {
        'password__field__fieldset__label--flat': props.flatView
    } );

    /**
     * @desc returns the array of criterion for passwordField
     */
    const passwordCriterion = getCriterionModel( props.criterion );

    let errorMessageView = props.showErrors && ! isEmpty( props.errorMessage ) ? (
        <p className='input__field--error-message'> { props.errorMessage } </p>
    ) : null;

    if( ! isEmpty( passwordCriterion ) ) {
        errorMessageView = (
            <div className='password__field--criterion'>
                <p className='password__field--criterion-title'>{ 'Contain atleast:' }</p>
                <div className='password__field--criterion__list'>
                    {
                        passwordCriterion.map( ( criterion ) => {
                            return <span key={ criterion.name } className={ 'password__field--criterion__list__item ' + getCriteriaStatus( props.fieldCriterionStatus[ criterion.name ] )}> { criterion.errorMessage } </span>;
                        } )
                    }
                </div>
            </div>
        );
    }

    return (
        <div key = { props.key } className = { _className }>
            <fieldset className = 'password__field__fieldset'>
                <TitleView
                    tagName = 'label'
                    level = { 3 }
                    value = { props.displayLabel }
                    className={ labelClass }
                />

                <TextFieldView
                    className = { _dynamicClass }
                    value = { props.displayValue }
                    { ...props }
                />

            </fieldset>
            { errorMessageView }
        </div>
    );
};

/**
 * @desc Returns the class for criteria if it is fulfilled to change its color to green
 * @param {*} status - gives the status of a criteria i.e. whether it is fulfilled or not
 */
const getCriteriaStatus = ( status ) => {
    if( status ) {
        return 'password__field--criterion__list__item__fulfilled';
    } else {
        return null;
    }
};

passwordFieldView.defaultProps = {
    type: 'text',
    displayLabel: '',
    required: false,
    readOnly: false,
    placeholder: '',
    showErrors: false
};

