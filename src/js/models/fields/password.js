import { COMMON, FIELD_PROPERTY_NAMES } from 'constants';

export default {
    id: 'password',
    name: 'password',
    type: FIELD_PROPERTY_NAMES.FIELD_TYPE.PASSWORD,
    value: '',
    placeholder: 'Enter password',
    required: true,
    criterion: 'password',
    displayLabel: 'Password',
    errorMessage: '',
	requiredMessage: 'Please enter password',
    config: {
		alsoValidateOnChange: [ 'confirmPassword' ]
	},
    isValid: false,
    validations: []
};
