import { FIELD_PROPERTY_NAMES, FIELD_VALIDATION_NAMES } from 'constants';

export default {
    id: 'username',
    name: 'username',
    type: FIELD_PROPERTY_NAMES.FIELD_TYPE.TEXT,
    value: '',
    placeholder: 'Enter email ID',
    required: true,
    validations: [ {
		name: FIELD_VALIDATION_NAMES.EMAIL,
		errorMessage: 'Please enter correct email'
	} ],
    displayLabel: 'Username',
    errorMessage: '',
	requiredMessage: 'Please enter email',
    config: {
		alsoValidateOnChange: [ 'confirmUsername' ]
	},
    isInputValid: true,
    criterion: null
};
