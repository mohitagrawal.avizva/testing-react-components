import { FIELD_PROPERTY_NAMES, FIELD_VALIDATION_NAMES } from 'constants';

export default {
    id: 'zipCode',
    name: 'zipCode',
    type: FIELD_PROPERTY_NAMES.FIELD_TYPE.TEXT,
    value: '',
    placeholder: 'Enter zip code',
    required: true,
    validations: [ { name: FIELD_VALIDATION_NAMES.ZIPCODE, errorMessage: 'Please enter correct zip code' } ],
    displayLabel: 'Zip Code',
    errorMessage: '',
	requiredMessage: 'Please enter zip code',
	config: {},
    isValid: true,
    criterion: null
};
