import username from './userName';
import confirmUsername from './confirmUserName';
import password from './password';
import confirmPassword from './confimPassword';
import zipCode from './zipCode';

export {
    username,
    confirmUsername,
    password,
    confirmPassword, 
	zipCode
};
