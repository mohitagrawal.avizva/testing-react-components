import {  FIELD_VALIDATION_NAMES, FIELD_PROPERTY_NAMES } from 'constants';

export default {
    id: 'confirmUsername',
    name: 'confirmUsername',
    type: FIELD_PROPERTY_NAMES.FIELD_TYPE.TEXT,
    value: '',
    placeholder: 'Re-enter email',
    required: true,
    validations: [
		{ name: FIELD_VALIDATION_NAMES.EQUAL_TO_CASE_IN_SENSITIVE, errorMessage: 'Should be same as username' }
	],
    displayLabel: 'Confirm Username',
    errorMessage: 'Invalid Format',
    requiredMessage: 'Please confirm email',
    config: {
		referenceFieldId: 'username'
	},
    isValid: false,
    criterion: null
};
