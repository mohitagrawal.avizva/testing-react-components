import { FIELD_PROPERTY_NAMES } from 'constants';

export default {
    id: 'confirmPassword',
    name: 'confirmPassword',
    type: FIELD_PROPERTY_NAMES.FIELD_TYPE.PASSWORD,
    value: '',
    placeholder: 'Re-enter password',
    required: true,
	validations: [],
	displayLabel: 'Confirm Password',
    errorMessage: '',
    requiredMessage: 'Please confirm password',
    config: {
		referenceFieldId: 'password'
	},
    isValid: true,
    criterion: 'confirmPassword'
};
