import {
    username,
    confirmUsername,
    password,
    confirmPassword, 
    zipCode
} from './fields';

const fieldsMap = {
    username,
    confirmUsername,
    password,
    confirmPassword, 
    zipCode
};

const fieldsOrder = [
    'username',
    'confirmUsername',
    'password',
    'confirmPassword', 
    'zipCode'
];
export {
    fieldsMap,
    fieldsOrder
};