
import { validateValue } from './validateValue';
import { forEach, isEmpty } from 'lodash';
import { getCriterionModel } from './getCriterionModel';

export const validateCriterion = (
    value,
    criterion,
    getReferenceValue,
    config
) => {

    const _answer = {};

    const _criterionModel = getCriterionModel( criterion );
    
    switch( criterion ) {
        
        case 'password':
        case 'confirmPassword': {
            
            forEach( _criterionModel, ( ( criterionItem )  => {
                
                const _hasPassed = validateValue(
                    value,
                    criterionItem,
                    getReferenceValue,
                    config
                );

                _answer[ criterionItem.name ] = _hasPassed;
            
            } ) );
            
            break;
        }
        
        default:
            break;
    }
    
    return isEmpty( _answer ) ? null : _answer;
};
