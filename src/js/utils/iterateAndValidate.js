import { validateValue } from './validateValue';

const validFieldResponse = {
    isValid: true,
    errorMessage: '',
    criterionStatus: null
};

/**
 * @desc Gets the field's value and list of validations as arguments.
 * Iterates over each validation object and evaluates it,
 * If it is found to be invalid, then simply returns.
 * If it is valid, moves over to the next validation until all validations are passed.
 * @param {*} payload
 */
export const iterateAndValidate = ( payload ) => {
    
    const { value, validations, getReferenceValue, config } = payload;
    
    let _ctr = 0;

    const _totalValidations = validations.length;

    for ( _ctr; _ctr < _totalValidations; _ctr = _ctr + 1 ) { //Iterates over all validations.

        const _validationObj = validations[ _ctr ]; //Current validation object in focus.

        if ( ! validateValue( value, _validationObj, getReferenceValue, config ) ) { //Validates the value against the validation and if false,
            return { //returns the invalid response structure.
                isValid: false,
                errorMessage: _validationObj.errorMessage
            };
        }

    }

    //If all validations pass, the following statement gets executed.
    //This means that the field has been found as valid and response corresponding to the same is returned.
    return validFieldResponse;
};
