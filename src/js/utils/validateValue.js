import { trim, isEmpty } from 'lodash';
const fieldValidationNames = {
    REQUIRED: 'required',
    ONE_UPPER_CASE: 'oneUppercase',
    ONE_NUMERIC: 'oneNumeric',
	MINLENGTH: 'minlength',
	MAXLENGTH: 'maxlength',
    NOT_EQUAL_TO_FIELD: 'notEqualToField',
    ZIPCODE: 'zipCode',
    PHONE: 'phone',
    EMAIL: 'email',
    LAST_4_SSN: 'last4Ssn',
	EQUAL_TO_FIELD: 'equalToField',
	EQUAL_TO_CASE_IN_SENSITIVE: 'equalToCaseInsensitive'
};

export const validateValue = ( value, validationObj, getReferenceValue, config ) => {

    let _valid = true;

    const { name } = validationObj; //validation Name

    switch( name ) {
        case fieldValidationNames.REQUIRED: {
            _valid = ! isEmpty( trim( value ) );
            break;
        }
        case fieldValidationNames.ZIPCODE: { //if validation name === zipCode then performs zipCode Validation
			if ( ! /^[0-9]{5}([\s-]?[0-9]{4})?$/.test( value ) ) {
				_valid = false;
			}
            break;
        }
        case fieldValidationNames.PHONE: { //if validation name === phone then performs phone no. Validation
            if ( ! /^[0-9]{10}$/.test( value ) ) {
                _valid = false;
            }
            break;
        }
        case fieldValidationNames.EMAIL: { //Checks if the provided value is a valid email value.
            _valid = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test( value );
            break;
        }
        case fieldValidationNames.LAST_4_SSN: { //Checks for the last 4 digits of the SSN.
            if ( ! /^[0-9]{4}$/.test( value ) ) {
                _valid = false;
            }
            break;
		}
        case fieldValidationNames.EQUAL_TO_FIELD: { //Checks if the value of the field is equal to that of the reference field.
			const _referencevalue = getReferenceValue( config.referenceFieldId );
            _valid = value === _referencevalue;
            break;
		}
		case fieldValidationNames.EQUAL_TO_CASE_IN_SENSITIVE: { //Checks if the value of the field is equal to that of the reference field.
            const _referencevalue = getReferenceValue( config.referenceFieldId );
            _valid = value.toLowerCase() === _referencevalue.toLowerCase();
            break;
        }
        case fieldValidationNames.ONE_UPPER_CASE: {
            _valid = /.*[A-Z].*/.test( value );
            break;
        }
        case fieldValidationNames.ONE_NUMERIC: {
            _valid = /.*[0-9].*/.test( value );
            break;
        }
        case fieldValidationNames.MINLENGTH: {
            const _value = trim( value );
            _valid = validationObj.minlength <= _value.length;
            break;
		}
		case fieldValidationNames.MAXLENGTH: {
            const _value = trim( value );
            _valid = validationObj.maxlength >= _value.length;
            break;
        }
        default:
            break;
}
return _valid;

};
