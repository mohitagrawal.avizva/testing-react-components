const passwordCriterion = [
    {
        name: 'required',
        priority: 1,
        errorMessage: 'Cannot be blank'
      },
      {
        name: 'oneUppercase',
        priority: 2,
        errorMessage: '1 capital letter'
      },
      {
        name: 'oneNumeric',
        priority: 3,
        errorMessage: '1 number'
      },
      {
        name: 'minlength',
        minlength: 8,
        priority: 4,
        errorMessage: '8 characters'
      }
];

const confirmPasswordCriterion = [
    ...passwordCriterion,
    {
        name: 'equalToField',
        priority: 5,
        errorMessage: 'Same as password'
    }

];


export const getCriterionModel = ( criterionName ) => {

    switch( criterionName ) {

        
        case 'password': {
            return passwordCriterion;
        }
        
        case 'confirmPassword': {
            return confirmPasswordCriterion;
        }
        
        default:
            break;

    }

};
