import { iterateAndValidate } from './iterateAndValidate';
import { isEmpty, trim } from 'lodash';
import { validateCriterion } from './validateCriterion';

const validFieldResponse = {
    isValid: true,
    errorMessage: '',
    criterionStatus: null
};

export const runFieldValidations = (
    payload
) => {

    const {
        value,
        validations,
        required,
        requiredMessage,
        criterion,
        getReferenceValue,
        config
    } = payload;

    //If there is a value that needs to be validated.
    const _isValueEmpty = isEmpty( trim( value ) );

    let _answer = validFieldResponse;

    if ( required ) { //If the field is a mandatory one.

        if ( _isValueEmpty ) { //If the value is empty, then mark invalid.

            _answer = {
                isValid: false,
                errorMessage: requiredMessage
            };

        } else { //Otherwise, make sure that the value is passing all the other given validations.

            _answer = iterateAndValidate( {
                value,
                validations,
                getReferenceValue,
                config
            } );

        }

    } else {

        if ( ! _isValueEmpty ) { //Even if the field is not mandatory, it should have value in the right format.

            _answer = iterateAndValidate( {
                value,
                validations,
                getReferenceValue,
                config
            } );

        } else { //If field is empty and not mandatory, simply mark it as a valid one.

            _answer = validFieldResponse;

        }
    }

    if ( ! isEmpty( criterion ) ) { //In case a field has a criterion name, it needs to be evaluated

        _answer.criterionStatus = validateCriterion(
            value,
            criterion,
            getReferenceValue,
            config
		);
		_answer.isValid = true;

        let _key;

        for ( _key in _answer.criterionStatus ) {
            if ( _answer.criterionStatus.hasOwnProperty( _key ) ) {
                if ( ! _answer.criterionStatus[ _key ] ) {
					_answer.isValid = false;
                    break;
                }
            }
        }
    }

    return _answer;

};
