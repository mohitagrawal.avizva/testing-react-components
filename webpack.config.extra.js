const path = require( 'path' );

// module alias
exports.alias = {
    
    'components': path.resolve( __dirname, 'src/js/components' ),
    'controllers': path.resolve( __dirname, 'src/js/controllers' ),
    'views': path.resolve( __dirname, 'src/js/views' ),
    'utils': path.resolve( __dirname, 'src/js/utils/' ),
    'constants': path.resolve( __dirname, 'src/js/constants' ),
    'jsons': path.resolve( __dirname, 'src/jsons' ),
    'models': path.resolve( __dirname, 'src/js/models' ),

};
