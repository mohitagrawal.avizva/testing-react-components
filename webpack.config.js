/**
 * Basic requirements go below.
 */

const path = require("path");
const HtmlWebpackPlugin = require( "html-webpack-plugin" );
const MiniCSSExtractPlugin = require( 'mini-css-extract-plugin' );
const { alias } = require( './webpack.config.extra' );

/**
 * Webpack configuration goes below.
 */

var configuration = {
  context: __dirname,

  entry: [
    "./src/js/index",
    './src/scss/index.scss'
  ],

  output: {
    path: path.join(__dirname, "/dist"),
    filename: "[name].js"
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        }
      },
      {
        test: /\.s?css$/,
        use: [
            MiniCSSExtractPlugin.loader,
            'css-loader?-url', // -url: do not parse url() imports as relative urls will fail
            'postcss-loader',
            {
                loader: 'sass-loader'
            }
        ]
},
    ]
  },

  // resolve files configuration
  resolve: {
    // module alias
    alias: alias
},
  plugins: [
      new HtmlWebpackPlugin( {
          template: "./src/index.html"
      } ),
      new MiniCSSExtractPlugin( {
        filename: 'css/style.css'
    } ),
    ],

    // generate source map
    devtool: ( 'cheap-module-eval-source-map' )
};

module.exports = configuration;
